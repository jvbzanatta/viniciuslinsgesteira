class AddPlaceToEvaluations < ActiveRecord::Migration[5.0]
  def change
    add_reference :evaluations, :place, index: true
  end
end
