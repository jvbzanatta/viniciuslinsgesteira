class Evaluation < ApplicationRecord
  belongs_to :user
  belongs_to :place

  validates :place_id, presence: true

  validates :internet,:service,:price,:accomodation,:noyse,:general, presence: true, numericality: { greater_than: 0,  lesser_than: 6}

  include DefaultSortingConcern
  include FulltextConcern
  include CaptionConcern

  cattr_accessor :fulltext_fields do
    []
  end

  def self.permitted_attributes
    return :internet,:food,:drink,:service,:price,:accomodation,:noyse,:general,:user_id, :place_id
  end
end
