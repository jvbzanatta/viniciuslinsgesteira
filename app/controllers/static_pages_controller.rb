class StaticPagesController < ApplicationController
  protect_from_forgery with: :exception

  def about
  end

end
