class User < ApplicationRecord
  has_many :evaluations
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, :email, :username, presence: true
  validates :username, uniqueness: { case_sensitive: false }
  validates :password, :presence => true, :on => :create
  validates :password_confirmation, :presence => true, :on => :create

end
