class AddUserToEvaluations < ActiveRecord::Migration[5.0]
  def change
    add_reference :evaluations, :user, index: true
  end
end
