class CreateEvaluations < ActiveRecord::Migration[5.0]
  def change
    create_table :evaluations do |t|
      t.integer :internet
      t.string :food
      t.string :drink
      t.integer :service
      t.integer :price
      t.integer :accomodation
      t.integer :noyse
      t.integer :general

      t.timestamps
    end
  end
end
