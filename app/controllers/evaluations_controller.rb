# encoding : utf-8


class EvaluationsController < BeautifulController

  before_action :load_evaluation, :only => [:show, :edit, :update]
  # Uncomment for check abilities with CanCan
  #authorize_resource

  def index
    session['fields'] ||= {}
    session['fields']['evaluation'] ||= (Evaluation.columns.map(&:name) - ["id"])[0..4]
    do_select_fields('evaluation')
    do_sort_and_paginate('evaluation')
    
    @q = Evaluation.ransack(
      params[:q]
    )

    @evaluation_scope = @q.result(
      :distinct => true
    ).sorting(
      params[:sorting]
    ).includes(:place)
    
    @evaluation_scope_for_scope = @evaluation_scope.dup
    
    unless params[:scope].blank?
      @evaluation_scope = @evaluation_scope.send(params[:scope])
    end
    
    @evaluations = @evaluation_scope.paginate(
      :page => params[:page],
      :per_page => 20
    ).to_a

    respond_to do |format|
      format.html{
        render
      }
      format.json{
        render :json => @evaluation_scope.to_a
      }
      format.csv{
        require 'csv'
        csvstr = CSV.generate do |csv|
          csv << Evaluation.attribute_names
          @evaluation_scope.to_a.each{ |o|
            csv << Evaluation.attribute_names.map{ |a| o[a] }
          }
        end 
        render :text => csvstr
      }
      format.xml{ 
        render :xml => @evaluation_scope.to_a
      }             
      format.pdf{
        pdfcontent = PdfReport.new.to_pdf(Evaluation,@evaluation_scope)
        send_data pdfcontent
      }
    end
  end

  def my_index
    session['fields'] ||= {}
    session['fields']['evaluation'] ||= (Evaluation.columns.map(&:name) - ["id"])[0..4]
    do_select_fields('evaluation')
    do_sort_and_paginate('evaluation')
    
    @q = Evaluation.where(user_id: current_user.id).ransack(
      params[:q]
    )

    @evaluation_scope = @q.result(
      :distinct => true
    ).sorting(
      params[:sorting]
    )
    
    @evaluation_scope_for_scope = @evaluation_scope.dup
    
    unless params[:scope].blank?
      @evaluation_scope = @evaluation_scope.send(params[:scope])
    end
    
    @evaluations = @evaluation_scope.paginate(
      :page => params[:page],
      :per_page => 20
    ).to_a

    respond_to do |format|
      format.html{
        render
      }
      format.json{
        render :json => @evaluation_scope.to_a
      }
      format.csv{
        require 'csv'
        csvstr = CSV.generate do |csv|
          csv << Evaluation.attribute_names
          @evaluation_scope.to_a.each{ |o|
            csv << Evaluation.attribute_names.map{ |a| o[a] }
          }
        end 
        render :text => csvstr
      }
      format.xml{ 
        render :xml => @evaluation_scope.to_a
      }             
      format.pdf{
        pdfcontent = PdfReport.new.to_pdf(Evaluation,@evaluation_scope)
        send_data pdfcontent
      }
    end
  end

  def show
    respond_to do |format|
      format.html{
        render
      }
      format.json { render :json => @evaluation }
    end
  end

  def new
    @places = Place.all.select(:id, :name)
    puts "Oee"
    puts @places
    @evaluation = Evaluation.new

    respond_to do |format|
      format.html{
        render
      }
      format.json { render :json => @evaluation }
    end
  end

  def edit
    @places = Place.all.select(:id, :name)
    
  end

  def create
    @places = Place.all.select(:id, :name)
    @evaluation = Evaluation.new(params_for_model)
    @evaluation.user = current_user

    respond_to do |format|
      if @evaluation.save
        format.html {
          if params[:mass_inserting] then
            redirect_to evaluations_path(:mass_inserting => true)
          else
            redirect_to evaluation_path(@evaluation), :flash => { :notice => t(:create_success, :model => "evaluation") }
          end
        }
        format.json { render :json => @evaluation, :status => :created, :location => @evaluation }
      else
        format.html {
          if params[:mass_inserting] then
            redirect_to evaluations_path(:mass_inserting => true), :flash => { :error => t(:error, "Error") }
          else
            render :action => "new"
          end
        }
        format.json { render :json => @evaluation.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update

    respond_to do |format|
      if @evaluation.update_attributes(params_for_model) and current_user.id == @evaluation.user.id
        format.html { redirect_to evaluation_path(@evaluation), :flash => { :notice => t(:update_success, :model => "evaluation") }}
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @evaluation.errors, :status => :unprocessable_entity }
      end
    end
  end

# def destroy
#   @evaluation.destroy

#   respond_to do |format|
#     format.html { redirect_to evaluations_url }
#     format.json { head :ok }
#   end
# end

  def batch
    attr_or_method, value = params[:actionprocess].split(".")

    @evaluations = []    
    
    Evaluation.transaction do
      if params[:checkallelt] == "all" then
        # Selected with filter and search
        do_sort_and_paginate(:evaluation)

        @evaluations = Evaluation.ransack(
          params[:q]
        ).result(
          :distinct => true
        )
      else
        # Selected elements
        @evaluations = Evaluation.find(params[:ids].to_a)
      end

      @evaluations.each{ |evaluation|
        if not Evaluation.columns_hash[attr_or_method].nil? and
               Evaluation.columns_hash[attr_or_method].type == :boolean then
         evaluation.update_attribute(attr_or_method, boolean(value))
         evaluation.save
        else
          case attr_or_method
          # Set here your own batch processing
          # evaluation.save
          when "destroy" then
            evaluation.destroy
          end
        end
      }
    end
    
    redirect_to :back
  end

  def treeview

  end

  def treeview_update
    modelclass = Evaluation
    foreignkey = :evaluation_id

    render :nothing => true, :status => (update_treeview(modelclass, foreignkey) ? 200 : 500)
  end
  
  private 
  
  def load_evaluation
    @evaluation = Evaluation.find(params[:id])
  end

  def params_for_model
    params.require(:evaluation).permit(Evaluation.permitted_attributes)
  end
end

