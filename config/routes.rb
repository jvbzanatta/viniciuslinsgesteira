Rails.application.routes.draw do
  root :to => 'beautiful#dashboard'
  match ':model_sym/select_fields' => 'beautiful#select_fields', :via => [:get, :post]

  concern :bs_routes do
    collection do
      post :batch
      get  :treeview, :my_index
      match :search_and_filter, :action => :index, :as => :search, :via => [:get, :post]
    end
    member do
      post :treeview_update
    end
  end
  authenticate :user do
    resources :evaluations, only: [:new, :create, :edit, :update, :my_index]
    resources :places, only: [:new, :create, :edit, :update]
  end
  # Add route with concerns: :bs_routes here # Do not remove
  resources :places, concerns: :bs_routes
  
  resources :evaluations, concerns: :bs_routes
  

  resources :static_pages
  devise_for :users, :evaluations

  root to: "application#index"

  get '/about', to: 'static_pages#about'
#  get '/my_index', to: 'evaluations#my_index'
end
