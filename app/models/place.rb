class Place < ApplicationRecord
  has_many :evaluations
  def self.inheritance_column
    'a'
  end

  validates :name, uniqueness: true

  include DefaultSortingConcern
  include FulltextConcern
  include CaptionConcern

  cattr_accessor :fulltext_fields do
    []
  end

  def self.permitted_attributes
    return :name,:address,:city,:state,:country,:type
  end
end
