# encoding : utf-8


class PlacesController < BeautifulController

  before_action :load_place, :only => [:show, :edit, :update]

  # Uncomment for check abilities with CanCan
  #authorize_resource

  def index
    session['fields'] ||= {}
    session['fields']['place'] ||= (Place.columns.map(&:name) - ["id"])[0..4]
    do_select_fields('place')
    do_sort_and_paginate('place')
    
    @q = Place.ransack(
      params[:q]
    )

    @place_scope = @q.result(
      :distinct => true
    ).sorting(
      params[:sorting]
    )
    
    @place_scope_for_scope = @place_scope.dup
    
    unless params[:scope].blank?
      @place_scope = @place_scope.send(params[:scope])
    end
    
    @places = @place_scope.paginate(
      :page => params[:page],
      :per_page => 20
    ).to_a

    respond_to do |format|
      format.html{
        render
      }
      format.json{
        render :json => @place_scope.to_a
      }
      format.csv{
        require 'csv'
        csvstr = CSV.generate do |csv|
          csv << Place.attribute_names
          @place_scope.to_a.each{ |o|
            csv << Place.attribute_names.map{ |a| o[a] }
          }
        end 
        render :text => csvstr
      }
      format.xml{ 
        render :xml => @place_scope.to_a
      }             
      format.pdf{
        pdfcontent = PdfReport.new.to_pdf(Place,@place_scope)
        send_data pdfcontent
      }
    end
  end

  def show
    respond_to do |format|
      format.html{
        render
      }
      format.json { render :json => @place }
    end
  end

  def new
    @place = Place.new

    respond_to do |format|
      format.html{
        render
      }
      format.json { render :json => @place }
    end
  end

  def edit
    
  end

  def create
    @place = Place.new(params_for_model)

    respond_to do |format|
      if @place.save
        format.html {
          if params[:mass_inserting] then
            redirect_to places_path(:mass_inserting => true)
          else
            redirect_to place_path(@place), :flash => { :notice => t(:create_success, :model => "place") }
          end
        }
        format.json { render :json => @place, :status => :created, :location => @place }
      else
        format.html {
          if params[:mass_inserting] then
            redirect_to places_path(:mass_inserting => true), :flash => { :error => t(:error, "Error") }
          else
            render :action => "new"
          end
        }
        format.json { render :json => @place.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update

    respond_to do |format|
      if @place.update_attributes(params_for_model)
        format.html { redirect_to place_path(@place), :flash => { :notice => t(:update_success, :model => "place") }}
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @place.errors, :status => :unprocessable_entity }
      end
    end
  end

# def destroy
#   @place.destroy

#   respond_to do |format|
#     format.html { redirect_to places_url }
#     format.json { head :ok }
#   end
# end

  def batch
    attr_or_method, value = params[:actionprocess].split(".")

    @places = []    
    
    Place.transaction do
      if params[:checkallelt] == "all" then
        # Selected with filter and search
        do_sort_and_paginate(:place)

        @places = Place.ransack(
          params[:q]
        ).result(
          :distinct => true
        )
      else
        # Selected elements
        @places = Place.find(params[:ids].to_a)
      end

      @places.each{ |place|
        if not Place.columns_hash[attr_or_method].nil? and
               Place.columns_hash[attr_or_method].type == :boolean then
         place.update_attribute(attr_or_method, boolean(value))
         place.save
        else
          case attr_or_method
          # Set here your own batch processing
          # place.save
          when "destroy" then
            place.destroy
          end
        end
      }
    end
    
    redirect_to :back
  end

  def treeview

  end

  def treeview_update
    modelclass = Place
    foreignkey = :place_id

    render :nothing => true, :status => (update_treeview(modelclass, foreignkey) ? 200 : 500)
  end
  
  private 
  
  def load_place
    @place = Place.find(params[:id])
  end

  def params_for_model
    params.require(:place).permit(Place.permitted_attributes)
  end
end

